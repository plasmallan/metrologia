# Metrología

Este repositorio almacena códigos y cuadernos de Jupyter relacionados con Metrología aplicada: Validaciones de Hojas de Cálculo, Validación de Métodos, Simulaciones en Monte Carlo para estimaciones de Incertidumbre, etc.